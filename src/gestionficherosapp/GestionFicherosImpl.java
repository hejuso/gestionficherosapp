package gestionficherosapp;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import gestionficheros.FormatoVistas;
import gestionficheros.GestionFicheros;
import gestionficheros.GestionFicherosException;
import gestionficheros.TipoOrden;

public class GestionFicherosImpl implements GestionFicheros {
	private File carpetaDeTrabajo = null;
	private Object[][] contenido;
	private int filas = 0;
	private int columnas = 3;
	private FormatoVistas formatoVistas = FormatoVistas.NOMBRES;
	private TipoOrden ordenado = TipoOrden.DESORDENADO;

	public GestionFicherosImpl() {
		carpetaDeTrabajo = File.listRoots()[0];
		actualiza();
	}

	private void actualiza() {

		String[] ficheros = carpetaDeTrabajo.list(); // obtener los nombres
		// calcular el n�mero de filas necesario
		filas = ficheros.length / columnas;
		if (filas * columnas < ficheros.length) {
			filas++; // si hay resto necesitamos una fila m�s
		}

		// dimensionar la matriz contenido seg�n los resultados

		contenido = new String[filas][columnas];
		// Rellenar contenido con los nombres obtenidos
		for (int i = 0; i < columnas; i++) {
			for (int j = 0; j < filas; j++) {
				int ind = j * columnas + i;
				if (ind < ficheros.length) {
					contenido[j][i] = ficheros[ind];
				} else {
					contenido[j][i] = "";
				}
			}
		}
	}

	@Override
	public void arriba() {
		if (carpetaDeTrabajo.getParentFile() != null) {
			carpetaDeTrabajo = carpetaDeTrabajo.getParentFile();
			actualiza();
		}
	}

	@Override
	public void creaCarpeta(String arg0) throws GestionFicherosException {
		File file = new File(carpetaDeTrabajo, arg0);

		// Se comprueba que la nueva carpeta no exista y la carpeta padre tenga permisos
		if (file.getParentFile().canWrite() && !file.exists()) {
			String nuevaCarpeta = file.getAbsolutePath();
			//Creamos la nueva carpeta en el directorio actual
			new File(nuevaCarpeta).mkdirs();
			actualiza();

		} else {
			if (!file.getParentFile().canWrite()) {
				throw new GestionFicherosException("No tienes permisos de escritura");
			} else if (file.exists()) {
				throw new GestionFicherosException("El directorio ya existe.");
			}
		}
	}

	@Override
	public void creaFichero(String arg0) throws GestionFicherosException {
		File file = new File(carpetaDeTrabajo, arg0);
		
		// Se comprueba que el nuevo fichero no exista y la carpeta padre tenga permisos
		if (file.getParentFile().canWrite() && !file.exists()) {
			String nuevaCarpeta = file.getAbsolutePath();
			try {
				//Creamos el nuevo fichero en el directorio actual
				new File(nuevaCarpeta).createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			actualiza();

		} else {
			if (!file.getParentFile().canWrite()) {
				throw new GestionFicherosException("No tienes permisos de escritura");
			} else if (file.exists()) {
				throw new GestionFicherosException("El archivo ya existe.");
			}
		}

	}

	@Override
	public void elimina(String arg0) throws GestionFicherosException {
		File file = new File(carpetaDeTrabajo, arg0);
		
		// Se comprueba que el fichero a eliminar exista y la carpeta padre tenga permisos
		if (file.getParentFile().canWrite() && file.exists()) {
			String nuevaCarpeta = file.getAbsolutePath();
			//Eliminamos el fichero en el directorio actual
			new File(nuevaCarpeta).delete();
			actualiza();

		} else {
			if (!file.getParentFile().canWrite()) {
				throw new GestionFicherosException("No tienes permisos de escritura");
			} else if (!file.exists()) {
				throw new GestionFicherosException("El archivo no existe.");
			}
		}

	}

	@Override
	public void entraA(String arg0) throws GestionFicherosException {
		File file = new File(carpetaDeTrabajo, arg0);

		if (!file.isDirectory()) {
			throw new GestionFicherosException(
					"Error. Se esperaba un directorio, pero " + file.getAbsolutePath() + " no es un directorio.");
		}

		if (!file.canRead()) {
			throw new GestionFicherosException(
					"Cuidado. No puedes acceder a " + file.getAbsolutePath() + ". No tienes permisos");
		}

		if (!file.canExecute()) {
			throw new GestionFicherosException(
					"Cuidado. No puedes acceder a " + file.getAbsolutePath() + ". No tienes permisos");
		}

		// actualizar carpeta de trabajo
		carpetaDeTrabajo = file;

		// actualizar contenido
		actualiza();
	}

	@Override
	public int getColumnas() {

		return columnas;
	}

	@Override
	public Object[][] getContenido() {
		return contenido;
	}

	@Override
	public String getDireccionCarpeta() {
		return carpetaDeTrabajo.getAbsolutePath();
	}

	@Override
	public String getEspacioDisponibleCarpetaTrabajo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getEspacioTotalCarpetaTrabajo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getFilas() {
		// TODO Auto-generated method stub
		return filas;
	}

	@Override
	public FormatoVistas getFormatoContenido() {
		return formatoVistas;
	}

	@Override
	public String getInformacion(String arg0) throws GestionFicherosException {
		File file = new File(carpetaDeTrabajo, arg0);
		StringBuilder stringBuilder = new StringBuilder();

		/*
		 * Controlar que existe, y si no que se lanze una excepción Controlar que haya
		 * permisos de lectura. Si no, excepción
		 * 
		 * He incluido el "file.getName().length() > 0" porque al seleccionar un espacio
		 * vacío en el directorio sale información vacía.
		 */

		if (file.exists() && file.canRead() && file.getName().length() > 0) {

			// Titulo
			stringBuilder.append("Información del sistema");
			stringBuilder.append("\n\n");

			// Nombre
			stringBuilder.append("Nombre: ");
			stringBuilder.append(arg0);
			stringBuilder.append("\n\n");

			// Tipo de fichero
			stringBuilder.append("Tipo: ");
			if (file.isDirectory()) {
				stringBuilder.append("Directorio");
			} else {
				stringBuilder.append("Fichero");
			}
			stringBuilder.append("\n\n");

			// Ubicación
			stringBuilder.append("Ubicación: ");
			stringBuilder.append(file.getAbsolutePath());

			stringBuilder.append("\n\n");

			// Fecha última modificación
			stringBuilder.append("Fecha de la última modificación: ");
			DateFormat fechaMod = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			String ultimaModificacion = fechaMod.format(file.lastModified());
			stringBuilder.append(ultimaModificacion);

			stringBuilder.append("\n\n");
			// Si el fichero está oculto
			stringBuilder.append("Fichero oculto: ");
			if (file.isHidden()) {
				stringBuilder.append("Oculto");
			} else {
				stringBuilder.append("No oculto");
			}

			stringBuilder.append("\n\n");
			// Si es directorio, mostrar el espacio libre y el espacio total (devolver en
			// bytes)

			if (file.isDirectory()) {
				long espacioLibre = file.getFreeSpace();
				long espacioTotal = file.getTotalSpace();

				stringBuilder.append("Espacio libre del directorio: ");
				stringBuilder.append(espacioLibre + " bytes");
				stringBuilder.append("\n");
				stringBuilder.append("Espacio Total del directorio: ");
				stringBuilder.append(espacioTotal + " bytes");
			} else {
				long tamanyo = file.length();
				stringBuilder.append("Tamaño del archivo: ");
				stringBuilder.append(tamanyo + " bytes");
			}

		} else {
			if (!file.exists() || file.getName().length() == 0) {
				throw new GestionFicherosException("Este fichero no existe.");
			}
			if (!file.canRead()) {
				throw new GestionFicherosException("Este fichero no tiene permisos de lectura.");
			}
		}

		return stringBuilder.toString();
	}

	@Override
	public boolean getMostrarOcultos() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getNombreCarpeta() {
		return carpetaDeTrabajo.getName();
	}

	@Override
	public TipoOrden getOrdenado() {
		return ordenado;
	}

	@Override
	public String[] getTituloColumnas() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getUltimaModificacion(String arg0) throws GestionFicherosException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String nomRaiz(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int numRaices() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void renombra(String arg0, String arg1) throws GestionFicherosException {
		File file = new File(carpetaDeTrabajo, arg0);
		// Nos guardamos en un tipo file el nombre nuevo
		File file2 = new File(carpetaDeTrabajo, arg1);
		// Comprobamos que la carpeta padre tenga permisos y si el nuevo nombre para el
		// archivo ya existe
		if (file.getParentFile().canWrite() && !file2.exists()) {
			//cambiamos el nombre
			file.renameTo(file2);
			actualiza();
		} else {
			if (!file.getParentFile().canWrite()) {
				throw new GestionFicherosException("No tienes permisos de escritura");
			}
			if (file2.exists()) {
				throw new GestionFicherosException("El archivo ya existe.");
			}
		}
	}

	@Override
	public boolean sePuedeEjecutar(String arg0) throws GestionFicherosException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean sePuedeEscribir(String arg0) throws GestionFicherosException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean sePuedeLeer(String arg0) throws GestionFicherosException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setColumnas(int arg0) {
		columnas = arg0;

	}

	@Override
	public void setDirCarpeta(String arg0) throws GestionFicherosException {
		File file = new File(arg0);

		// actualizar carpeta de trabajo
		carpetaDeTrabajo = file;

		if (!file.isDirectory()) {
			throw new GestionFicherosException(
					"Error. Se esperaba un directorio, pero " + file.getAbsolutePath() + " no es un directorio.");
		}

		if (!file.canRead()) {
			throw new GestionFicherosException(
					"Cuidado. No puedes acceder a " + file.getAbsolutePath() + ". No tienes permisos");
		}

		// actualizar contenido
		actualiza();
	}

	@Override
	public void setFormatoContenido(FormatoVistas arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setMostrarOcultos(boolean arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setOrdenado(TipoOrden arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setSePuedeEjecutar(String arg0, boolean arg1) throws GestionFicherosException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setSePuedeEscribir(String arg0, boolean arg1) throws GestionFicherosException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setSePuedeLeer(String arg0, boolean arg1) throws GestionFicherosException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setUltimaModificacion(String arg0, long arg1) throws GestionFicherosException {
		// TODO Auto-generated method stub

	}
}
